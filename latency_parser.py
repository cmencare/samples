import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from pandas import *
from matplotlib.pyplot import *
from seaborn import *


def changeheaders():
    for cols in latency.columns:
        if entry[cols].get() != "":
            latency.rename(columns={cols: entry[cols].get()}, inplace=True)
    if not entry:
        messagebox.showinfo('Error', 'List empty. This should never show up.')
        exit()
    entrywindow.quit()
    graphdata(latency)
    statdata(latency)


def headergui(latency):
    global entry
    global entrywindow

    if latency.empty:
        messagebox.showinfo('Error', 'Data frame lost data. This should never show up.')
        exit()
    entrywindow = tk.Tk()
    entrywindow.title('Alter Header Labels')
    tk.Label(entrywindow, font='-family Helvetica -weight bold', text='Alter Headers').pack(side='top')
    tk.Label(entrywindow, text='Below are the headers that will be used in graph and table generation.', font='Helvetica 9').pack(side='top')
    tk.Label(entrywindow, text='Please note: Graph generation can take time.', font='Helvetica 8 italic').pack(side='top')
    i = 0
    entry = {}
    for cols in latency.columns:
        e = tk.Entry(entrywindow, width=55)
        e.insert(0, cols)
        if 'Total Latency' in cols:
            e.configure(state='disabled')
        e.pack(side='top', padx=5, pady=1)
        entry[cols] = e
        i += 1
    tk.Button(entrywindow, text='Alter Headers', command=changeheaders).pack(side='left', padx=5, pady=5)
    tk.Button(entrywindow, text='Cancel', command=quit).pack(side='left', padx=5, pady=5)
    entrywindow.mainloop()


def ParseFile(file):
    global latency

    latency = concat(read_csv(f, header=0, skiprows=9, skipfooter=5, engine='python') for f in file)
    if 'DEX_TIMESTAMP' in latency.columns:
        latency['Datetime'] = to_datetime(latency['DEX_TIMESTAMP'], unit='ms')
        latency.set_index('Datetime', inplace=True)
    else:
        messagebox.showinfo('Error', 'Imported data does not have a DEX_TIMESTAMP header.')

    for cols in latency.columns:
        if cols.startswith('DEX_TIMESTAMP'):
            latency.drop(cols, 1, inplace=True)
        elif cols.startswith('clientTime'):
            latency.drop(cols, 1, inplace=True)
        elif cols.startswith('Message Count'):
            latency.drop(cols, 1, inplace=True)

    headergui(latency)


def ReParseFile(file):
    pass


def GetRawFiles():
    allfiles = filedialog.askopenfilenames(initialdir='C:\\PATH\\TO\\RAW\\DATA\\',
                                           filetypes=[('CSV Files', '.csv')])
    if allfiles:
        ParseFile(allfiles)
        mainwindow.quit()
    else:
        messagebox.showinfo('Error', 'No data imported.')
        mainwindow.quit()


def GetProcessedFiles():
    processedfile = filedialog.askopenfilename(initialdir='C:\\PATH\\TO\\PROCESSED\\DATA\\',
                                           filetypes=[('CSV Files', '.csv')])
    if processedfile:
        ReParseFile(processedfile)
        mainwindow.quit()
    else:
        messagebox.showinfo('Error', 'No data imported.')
        mainwindow.quit()


def StartApp():
    global mainwindow
    mainwindow = tk.Tk()
    mainwindow.title('Latency Parser')
    mainwindow.geometry('450x175')

    centerframe = tk.Frame(mainwindow, width=350, height=175)
    centerframe.pack(side='top', expand='NO', fill='none')
    tk.Label(centerframe, font='-family Helvetica -size 25', text='Latency Parser').pack(side='top', pady=0)

    tk.Label(centerframe, font='-family Helvetica -weight bold', text='Instructions').pack(side='top')
    tk.Label(centerframe, text='Select Raw Data: Import and process data from the Simple Latency tool', font='Helvetica 9').pack(side='top')
    tk.Label(centerframe, text='Select Processed CSV: Reprocess data from a results file this tool returns', font='Helvetica 9').pack(
        side='top')
    tk.Label(centerframe, text='Note: Data import can take a few minutes if large files are selected', font='Helvetica 8 italic').pack(
        side='top')

    tk.Button(centerframe, text='Select Raw CSV Files', command=GetRawFiles, width=20).pack(side='left', padx=10, pady=10)
    tk.Button(centerframe, text='Select Processed CSV Files', command=GetProcessedFiles, width=25).pack(side='left', padx=10, pady=10)
    tk.Button(centerframe, text='Cancel', command=quit, width=15).pack(side='left', padx=10, pady=10)

    mainwindow.mainloop()
    mainwindow.quit()


def graphdata(latency):
    for index in latency.columns:
        if (index.startswith('Total Latency')) or (index.startswith('Latency between')):
            graph = latency.plot(y=index, figsize=(10, 7), ylim=0, legend=False)
            graph.set_title(index)
            graph.set_ylabel('Latency (ms)')
            graph.set_xlabel('Date/Time')
            filename = index + '.png'
            savefig(filename, bbox_inches='tight')


def statdata(latency):
    total_latency = latency['Total Latency']
    totalmsg = total_latency.count()
    over1kms = (total_latency > 1000).values.sum()
    over10kms = (total_latency > 10000).values.sum()
    perc1kms = float("{0:.2f}".format((100*float(totalmsg-over1kms)/(totalmsg))))
    msgstats = {'Total number of messages': totalmsg, 'Number of messages over 1000 ms': over1kms, 'Number of messages over 10,000 ms': over10kms, 'Percent of messages under 1,000 ms': perc1kms}
    print (msgstats)

StartApp()
