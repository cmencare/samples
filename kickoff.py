import main

client = main.mongodb_conn_create()
channel = main.rabbitmq_conn_create()
steamid = "76561198019267094"

try:
    user_data = main.steam_get_user(steamid)
    main.rabbitmq_publish(channel, 'UserData', 'SteamUserData', user_data)
finally:
    main.mongodb_conn_close(client)
    main.rabbitmq_conn_close(channel)
