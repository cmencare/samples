import requests
import json
from datetime import timedelta, date, datetime
from pymongo import MongoClient

client = MongoClient("mongodb://user:pass@mongodb.mencarelli.local:27017/NYT")
db = client.NYT
df = []
#YEAR, MONTH, DAY
startdate = date(2018, 3, 25).isoformat()
i = 0



while i < 520:
    lastweek = datetime.strptime(startdate, "%Y-%m-%d") - timedelta(weeks=i)
    print(lastweek.date())
    url = "https://api.nytimes.com/svc/books/v3/lists//.json"
    params = {
        'api-key': "APIKEY",
        'list-name': "hardcover-fiction",
        'published-date': lastweek.strftime("%Y-%m-%d")}
    r = requests.get(url, params=params).json()
    jsondata = r['results']
    for row in jsondata:
        result = db.BestSellers.insert_one(row)
        print ("Inserted week: " + lastweek.strftime("%Y-%m-%d") + " + & id: " + str(result.inserted_id))
    i += 1


