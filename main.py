import pika
import pymongo
import requests
import json


def rabbitmq_conn_create():
    parameters = pika.URLParameters('amqp://user:pass@rabbitmq:5672')
    connection = pika.BlockingConnection(parameters)
    chan = connection.channel()
    chan.exchange_declare(exchange='Steam', type='direct')
    return chan


def rabbitmq_conn_close(connection):
    connection.close()


def rabbitmq_publish(chan, routingkey, queue_name, msg):
    queue = chan.queue_declare(queue=queue_name, durable=True)
    chan.queue_bind(exchange='Steam', queue=queue.method.queue, routing_key=routingkey)
    chan.basic_publish(exchange='Steam',
                       routing_key=routingkey,
                       body=msg,
                       properties=pika.BasicProperties(content_type='application/json',
                                                       delivery_mode=2
                                                       )
                       )


def rabbitmq_consume(ch, queue):
    method, header, body = ch.basic_get(queue)
    ch.basic_ack(method.delivery_tag)
    return body


def mongodb_conn_create():
    cli = pymongo.MongoClient(host='mongodb', port=27017)
    return cli


def mongodb_check_user(cli, steamid):
    count = cli.SteamFriends.FriendData.find({"steamid": steamid}).count()
    return count


def mongodb_conn_close(cli):
    cli.close()


def mongodb_add_user(cli, userdata):
    db = cli.SteamFriends
    jsonbody = json.loads(userdata)
    result = db['UserData'].insert_one(jsonbody)
    return result


# todo insert steam friend data



def steam_get_user(steamid):
    api_key = 'APIKEY'  ## API Key provided by Steam
    player_uri = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' + api_key + '&steamids=' + str(steamid)
    player_data = requests.get(player_uri).json()
    msg = json.dumps(player_data)
    return msg


def steam_get_friends(steamid):
    api_key = 'APIKEY'  ## API Key provided by Steam
    player_uri = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=' + api_key + '&steamids=' + str(steamid)
    friend_data = requests.get(player_uri).json()
    msg = json.dumps(friend_data)
    return msg